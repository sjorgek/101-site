# 101 - Criação de um sistema web simples

### 1. Adicionar as dependências do projeto

**1.1** Abra o arquivo pom.xml que está na raiz do projeto.

**1.2** Inclua a referência ao Spring Boot antes da tag dependencies. Isso determina a versão global do framework que será usada no projeto:

```
<parent>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-parent</artifactId>
  <version>2.0.3.RELEASE</version>
</parent>
```

**1.3** Apague o conteúdo da tag dependencies e inclua duas dependências: o móldulo web do Spring Boot e o módulo de testes.

```
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-test</artifactId>
	<scope>test</scope>
</dependency>
```

(Fique atento que as duas dependências devem ficar dentro da tag dependencies)

**1.4** Salve o arquivo. Imediatamente, o eclipse irá executar o comando do Maven que baixa e atualiza todas as dependências do projeto. Isso pode demorar alguns minutos para finalizar.


### 2. Inicializar a aplicação com Spring Boot

**Obs:** Toda classe ou função referenciada no código precisa ser declarada no mesmo arquivo ou importado de um arquivo externo (com excessão de algumas features básicas da linguagem). Lembre-se de usar o autocomplete do Eclipse para que a importação seja feita de forma correta.

**2.1** Abra o arquivo *App.java* localizado em *src/java/com/mastertech/site*.

**2.2** Inclua o annotation *@SpringBootApplication* que configura essa classe como um App do Spring Boot logo acima da declaração da classe.

```
@SpringBootApplication
public class App
```

**2.3** Substitua todo o conteúdo da função *main* pela linha abaixo. Essa instrução inicializa toda a aplicação e é indispensável para o seu funcionamento.

```
SpringApplication.run(App.class, args);
```

**2.4** Já podemos testar a aplicação, ainda que ela não tenha nenhuma funcionalidade programada. Para isso, vamos clicar com o botão direito do mouse sobre o arquivo *App.java*, selecionar *Run as* e depois *Java application*. Podemos ver que a inicialização foi bem sucedida quando vemos no console a mensagem *App started in ... seconds*

### 3 Adicionando conteúdo web

**3.1** Para criar uma resposta em uma URI da nossa aplicação, precisamos usar um tipo de objeto especial chamado de *Controller*. Vamos criá-lo clicando com o botão direito sobre o pacote *com.mastertech.site** e clicando em *New* e depois em *Class*.

**3.2** Vamos criar um novo pacote para comportar nossos controllers. Para isso, escreva no campo *Package* o nome *com.mastertech.site.controllers*.

**3.3** No campo *Name* digite o nome do controller: *SiteController*, e clique em *Finish*. Isso irá criar um novo arquivo e um novo pacote.

**3.4** A classe que criamos deve ser anotada com um **@RestController** para que ela seja identificada corretamente pelo Spring Boot.

```
@RestController
public class SiteController
```

**3.5** Cada URI da aplicação corresponde a um método de uma classe controller devidamente anotado. Adicione o código abaixo para exibir um texto *Olá Mundo* na home page do site.

```
@GetMapping("/")
public String home() {
	return "<h1>Olá Mundo</h1>";
}
```

**3.6** Podemos criar outras respostas em diferentes URIs da aplicação, o que corresponde a diversas páginas na aplicação cliente.

```
@GetMapping("/sobre")
public String about() {
	return "<h1>Essa é uma nova página</h1>" +
        "<a href='/'>Clique aqui</a> e volte para home";
}
```

**3.7** Esse é um exemplo de um conteúdo HTML gerado dinamicamente. Podemos ver que o conteúdo HTML não precisa existir de forma estática, a aplicação pode gerar uma resposta dinâmica em uma mesma URI.

```
@GetMapping("/saudacao")
public String greet() {
	String greetings[] = {"Olá!", "Saudações!", "Bem vindo!"};

	int random = (int) Math.floor(Math.random() * greetings.length);

	return greetings[random];
}
```
