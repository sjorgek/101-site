package com.mastertech.site.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SiteController 
{

	@GetMapping("/")
	public String home() {
		return "<h1>Olá Mundo</h1>";
	}
	
	@GetMapping("/sobre")
	public String about() {
		return "<h1>Essa é uma nova página</h1>" +
	        "<a href='/'>Clique aqui</a> e volte para home";
	}
	
	@GetMapping("/saudacao")
	public String greet() {
		String greetings[] = {"Olá!", "Saudações!", "Bem vindo!"};
		
		int random = (int) Math.floor(Math.random() * greetings.length);
		
		return greetings[random];
	}
}
